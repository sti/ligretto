
var App = angular.module('ligrettoApp',[]);


App.controller('LigrettoController',function($scope) {
	
  var ws = new WebSocket("ws://localhost:9000/ws");
  
  ws.onopen = function(){  
	  console.log("Socket has been opened!");  
	  
  };

  ws.onmessage = function(message) {
	var jsMsg =  JSON.parse(message.data);
	console.log(jsMsg);
	if (jsMsg.message === "refresh") {
		$scope.handSet = jsMsg.data.handSet;
		$scope.tableState = jsMsg.data.tableState;
		$scope.$apply();	
	}
  };
  
  $scope.reveal = function() {
	  ws.send(JSON.stringify("reveal"))
  }; 
  
  $scope.playCard = function(card) {
	  var arg = {card: card};
	  if ($scope.selectedStackId !== null) {
		  arg.stackId = $scope.selectedStackId
	  }
	  ws.send(JSON.stringify(arg));
  };
  
  $scope.selectedStackId = null;
  
  $scope.toggleStackSelection = function(id) {
	  if ($scope.selectedStackId === id) {
		  $scope.selectedStackId = null;
  	  } else {
  		  $scope.selectedStackId = id;
  	  } 
  }
});
	