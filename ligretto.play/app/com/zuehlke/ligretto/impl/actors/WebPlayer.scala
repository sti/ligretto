package com.zuehlke.ligretto
package impl.actors

import api.domain.{Card, HandSet}
import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.json.JsValue
import play.api.libs.json.JsString
import play.api.libs.json.JsObject
import play.api.libs.json.JsArray
import play.api.libs.json.JsNumber
import play.Logger
import play.api.libs.json.JsNull
import com.zuehlke.ligretto.api.domain.TableState
import com.zuehlke.ligretto.api.actors.PlayerBase
import com.zuehlke.ligretto.api.actors.PlayCard
import com.zuehlke.ligretto.api.actors.RevealNextCard
import com.zuehlke.ligretto.api.actors.CustomMessage
import play.api.libs.json.JsArray

class WebPlayer(channel: Channel[JsValue]) extends PlayerBase {
  
  override def receive = super.receive orElse { 
    case PlayCard(stackId, card) => 
      playCard(card, stackId)
    case RevealNextCard =>
      revealNextCard
      channel.push(createRefreshJsonMessage)
  }
  
  def onDealCards: Unit = {
    channel.push(createRefreshJsonMessage)
  }

  def onLigrettoStart: Unit = {
    channel.push(createJsonMessage("start"))
  }

  def onLigrettoStop: Unit = {
    channel.push(createJsonMessage("stop"))
  }

  def onCardAccepted: Unit = {
    channel.push(createJsonMessage("card-accepted"))
  }

  def onCardDenied: Unit = {
    channel.push(createJsonMessage("card-denied"))
  }

  def onCardPlayed(stackId: Int, card: Card): Unit = {
    channel.push(createRefreshJsonMessage)
  }
  
  def onCustomMessage(msg : CustomMessage) = {}
  
  def onHandSetRefresh : Unit = {
    channel.push(createRefreshJsonMessage)
  }
  

  private def createRefreshJsonMessage = {
    createJsonMessage("refresh", JsObject(Seq(
        "handSet" -> createHandSetJson(handSet), 
        "tableState" -> createTableStateJson(tableState)
    )))
  }

  private def createJsonMessage(message: String, data: JsValue = JsNull) = {
    JsObject(Seq(
      "message" -> JsString(message),
      "data" -> data
    ))
  }
  
  
  private def createHandSetJson(handSet: HandSet): JsValue = {
    JsObject(
      Seq(
        "open" -> JsArray(handSet.openCards.map(createJsonCard)),
        "topStack" -> createJsonCard(handSet.topStackCard),
        "revealed" -> (handSet.revealedHandCard match {
          case null => JsNull
          case c => createJsonCard(c)
        })
      )  
    )
  }
  
    
  private def createTableStateJson(tableState: TableState): JsValue = { 
    if (tableState == null || tableState.stackStates == null) {
      JsArray()
    }
    else {
	    JsArray(tableState.stackStates.toSeq.map { case (stackId, topCard) =>
	      JsObject(Seq(
	          "stackId" -> JsNumber(stackId),
	          "topCard" -> createJsonCard(topCard)
	      ))
	    })
    }
  }

  private def createJsonCard(card: Card): JsValue = {
    JsObject(
      Seq(
        "number" -> JsNumber(BigDecimal(card.number)),
        "color" -> JsString(card.color.toString),
        "playerId" -> JsNumber(BigDecimal(card.playerId))))
  }
}
