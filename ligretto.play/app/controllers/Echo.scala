package controllers

import play.api._
import play.api.mvc._
import play.api.libs.iteratee._	
import play.api.libs.json.JsValue

object Echo extends Controller {
  
	def simpleIterateeWebSocket = WebSocket.using[String] { request =>
	  val out = Enumerator.imperative[String]()
	  //val out = Enumerator("hhh")
	  //val in = Iteratee.foreach[String](println).mapDone { _ =>
//	  val in = Iteratee.foreach[String](str => Logger.info(str)).mapDone { _ =>
//	    //msg => out.push(msg)
//	    Logger.info("finish")
//	  }
	  
	    val in = Iteratee.foreach[String] {
		  msg =>
		  Logger.info(msg)  
		  out.push(msg)
	  }
	  (in, out)
	}
	
	//	 def editFeed(user:String) = WebSocket.async[JsValue] { request =>
	//        ChangeFeed.join(user)
	//    }
	//  
}