package controllers

import akka.actor._
import scala.concurrent.duration._
import play.api._
import play.api.libs.json._
import play.api.libs.iteratee._
import play.api.libs.concurrent._
import akka.util.Timeout
import akka.pattern.ask
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc.Controller
import play.api.mvc.Action
import play.api.mvc.WebSocket
import com.zuehlke.ligretto.api.GameFactory
import com.zuehlke.ligretto.api.actors.RequestJoin
import com.zuehlke.ligretto.impl.actors.WebPlayer
import com.zuehlke.ligretto.api.actors.PlayCard
import com.zuehlke.ligretto.api.domain.Card
import com.zuehlke.ligretto.api.domain.Color
import models._
import akka.actor._
import scala.concurrent.duration._
import com.zuehlke.ligretto.api.actors.PlayerDenied
import com.zuehlke.ligretto.api.actors.PlayerAccepted
import com.zuehlke.ligretto.api.actors.RevealNextCard

object Application extends Controller {
    
  def connectWs() = WebSocket.async[JsValue] { request => 
    LigrettoWebsocket.join()
  }
  
  def ligretto = Action {
    Ok(views.html.ligretto())
  }  
  
}


object LigrettoWebsocket {

    implicit val timeout = Timeout(1 second)

  def join() = {
    GameFactory.setPlayerCount(1)
    val supervisor = GameFactory.playerSupervisor(Akka.system)
    val (out, channel) = Concurrent.broadcast[JsValue]

    (supervisor ? RequestJoin(Props(new WebPlayer(channel)))).map {
      case PlayerAccepted(player) =>
        val in = Iteratee.foreach[JsValue] { jsonMsg =>
          val msg = parseJsonMessage(jsonMsg)
          player ! msg
        }

        (in, out)
      case PlayerDenied =>
        val in = Iteratee.foreach[JsValue] { event => }

        (in, out)
    }
  }
  
  private def parseJsonMessage(jsonMsg: play.api.libs.json.JsValue): Any = {
      val msg = jsonMsg.asOpt[String] match {
         case Some("reveal") => 
           RevealNextCard
         case None => 
           val cardJson = jsonMsg \ "card"
	       val card = parseJsonCard(cardJson)

	       (jsonMsg \ "stackId").asOpt[Int] match {
	         case Some(stackId) => PlayCard(stackId, card)
	         case None => PlayCard(null, card)
	       }
         case _ => throw new Exception("invalid message: " + jsonMsg.toString)
       }
      msg
    }
    
    private def parseJsonCard(cardJson: JsValue): Card = {
	    val playerId = (cardJson \ "playerId").as[Int]
	    val color = Color.withName((cardJson \ "color").as[String])
	    val number = (cardJson \ "number").as[Int]
	    
	    Card(playerId, color, number)
    }
}
