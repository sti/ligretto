import sbt._
import Keys._
import play.Project._

object LigrettoBuild extends Build {

  val appName         = "ligretto"
  val appVersion      = "1.0-SNAPSHOT"

   val appDependencies = Seq(
    //if it's a java project add javaCore, javaJdbc, jdbc etc.
  )

    lazy val ligretto = play.Project(appName, appVersion, appDependencies,
                           path = file("ligretto.play")) dependsOn(ligrettoAkka)

    lazy val ligrettoAkka = Project(id = "ligretto-akka",
                           base = file("ligretto.scala"))
}