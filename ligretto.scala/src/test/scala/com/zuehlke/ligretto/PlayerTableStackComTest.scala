package com.zuehlke.ligretto

import org.scalatest.FunSuite
import akka.actor.{Props, ActorSystem}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.zuehlke.ligretto.impl.actors.{TableStackSupervisor, PlayerSupervisor}
import com.zuehlke.ligretto.api.GameFactory
import com.zuehlke.ligretto.api.actors.{ComputerPlayer, RequestJoin}
import com.zuehlke.ligretto.api.actors.FozzyPlayer

@RunWith(classOf[JUnitRunner])
class PlayerTableStackComTest extends FunSuite {

  val system = ActorSystem("TestLigretto")

  test("bootStrapPlayers") {
    GameFactory.setPlayerCount(4)
    GameFactory.playerSupervisor(system) ! new RequestJoin(Props[FozzyPlayer])
    GameFactory.playerSupervisor(system) ! new RequestJoin(Props[FozzyPlayer])
    GameFactory.playerSupervisor(system) ! new RequestJoin(Props[FozzyPlayer])
    GameFactory.playerSupervisor(system) ! new RequestJoin(Props[FozzyPlayer])
    Thread.sleep(100000)
  }

}
