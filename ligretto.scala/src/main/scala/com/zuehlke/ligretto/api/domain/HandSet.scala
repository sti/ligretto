package com.zuehlke.ligretto.api.domain

trait HandSet {

  def revealedHandCard : Card
  def handCardSize : Int
  def stackSize : Int
  def topStackCard : Card
  def openCards : List[Card]
  def firstRevealHandCardCount : Int
  def isFirstRevealNextCard : Boolean
  def isPlayable(card : Card) : Boolean

}


