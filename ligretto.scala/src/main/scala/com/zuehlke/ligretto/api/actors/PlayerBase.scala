package com.zuehlke.ligretto.api.actors

import akka.actor.Actor
import com.zuehlke.ligretto.api.domain.{HandSet, TableState, Card}
import com.zuehlke.ligretto.impl.actors.ActorRegistry

abstract class PlayerBase extends Actor {

  private var _tableState: TableState = null
  private var _handSet: HandSet = null

  protected def tableState = {
    _tableState
  }

  protected def handSet = {
    _handSet
  }

  def receive = {
    case DealCards(handSet) => {
      this._handSet = handSet
      onDealCards
      requestStart
    }
    case LigrettoStart(initialTableState) => {
      debug("Received LigrettoStart")
      this._tableState = initialTableState
      onLigrettoStart
    }
    case CardAccepted(handSet) => {
      this._handSet = handSet
      onCardAccepted
    }
    case CardDenied => {
      onCardDenied
    }
    case CardPlayed(stackId, card, tableState) => {
      _tableState = tableState
      onCardPlayed(stackId, card)
    }
    case LigrettoStop => {
      onLigrettoStop
    }
    case HandSetRefresh(handSet) => {
      this._handSet = handSet
      onHandSetRefresh
    }
    case msg : CustomMessage => {
      onCustomMessage(msg)
    }
  }

  def playCard(card: Card) {
    context.parent ! PlayCard(null, card)
  }

  def playCard(card: Card, stackId: Int) {
    context.parent ! PlayCard(stackId, card)
  }

  def requestStart = {
    context.parent ! RequestStart
  }

  def setFirstCard(cardIndex: Int) = {
    context.parent ! new SetFirstCard(cardIndex)
  }

  def revealNextCard() = {
    context.parent ! RevealNextCard
  }


  private def debug(msg: String): Unit = {
    println("PlayerBase: " + msg)
  }

  def sendStop = {
    context.parent ! LigrettoStop
  }

  def onDealCards: Unit

  def onLigrettoStart: Unit

  def onLigrettoStop: Unit

  def onCardAccepted: Unit

  def onCardDenied: Unit

  def onCardPlayed(stackId: Int, card: Card): Unit
  
  def onCustomMessage(msg : CustomMessage) : Unit

  def onHandSetRefresh : Unit

}
