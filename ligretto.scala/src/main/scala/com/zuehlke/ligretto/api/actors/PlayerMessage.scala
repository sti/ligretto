package com.zuehlke.ligretto.api.actors

import com.zuehlke.ligretto.api.domain.{HandSet, TableState, Card}
import akka.actor.{ActorRef, Props}

trait MessageToPlayerGuardian // Messages to the player guardian

sealed trait PlayerMessage extends MessageToPlayerGuardian
case class PlayCard(stackId: Integer, card: Card) extends PlayerMessage

case class SetFirstCard(cardIndex: Int) extends PlayerMessage
case object RevealNextCard extends PlayerMessage

case object LigrettoStop extends PlayerMessage

sealed trait TableMessage // Messages from the player guardian
case class DealCards(handSet: HandSet) extends TableMessage
case class LigrettoStart(initialTableState: TableState) extends TableMessage
case class CardAccepted(handSet: HandSet) extends TableMessage
case object CardDenied extends TableMessage
case class CardPlayed(stackId: Int, card: Card, tableState: TableState) extends TableMessage

sealed trait PlayerRequest // Messages to the player guardian
case class RequestJoin(playerActorPros: Props) extends PlayerRequest
case object RequestStart extends PlayerRequest with MessageToPlayerGuardian

sealed trait PlayerResponse // Messages from the player supervisor
case class PlayerAccepted(actor: ActorRef) extends PlayerResponse
case class HandSetRefresh(handSet : HandSet) extends PlayerResponse
case object PlayerDenied extends PlayerResponse

trait CustomMessage
