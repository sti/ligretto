package com.zuehlke.ligretto.api.domain


trait TableState {

  def isEmpty = {
    stackStates.isEmpty
  }

  def stackStates: Map[Int, Card]

}
