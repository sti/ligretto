package com.zuehlke.ligretto.api.actors

import com.zuehlke.ligretto.api.domain.Card

case class TryPlayCard(stackId : Int, card : Card) extends CustomMessage
case class TryPlayStartingCard(card : Card) extends CustomMessage

class ComputerPlayer extends PlayerBase {
  
  private var isPlayingCard = false

  def onDealCards = {
	// do nothing
  }

  def onLigrettoStart {
    println("Start move detected, looking for cards with number 1")
    val card = handSet.openCards.find {
      c => c.number == Card.minNumber
    }
    if (card != None) {
      println("Try to create table stack with card: " + card.head)
      isPlayingCard = true
      playCard(card.head)
    }  
    playStartingCardsInHandCards
  }

  def onLigrettoStop = {}

  def onCardAccepted = {
    println("COMPUTERPLAYER::::::::CARD ACCEPTED")
    isPlayingCard = false
  }

  def onCardDenied = {
    println("COMPUTERPLAYER::::::::CARD DENIED")
    isPlayingCard = false
  }

  def onCardPlayed(stackId: Int, card: Card) = {
    if (card.number != Card.maxNumber) {
      var playableCard = getPlayableCardInOpenCards(card)
      if (playableCard == null) {
        playableCard = getPlayableCardInHandCards(card)
      }
      if (playableCard != null) {
    	if (isPlayingCard) {
    	  println("COMPUTERPLAYER:::::::::Cannot play card "+card+" to "+stackId+" yet, retrying later.")
    	  self ! new TryPlayCard(stackId, playableCard)
    	} else {
    	  println("COMPUTERPLAYER:::::::::Playing card "+card+" to "+stackId)
    	  isPlayingCard = true          
    	  playCard(playableCard, stackId)
    	}
      }
    }
  }
  
  def onCustomMessage(msg : CustomMessage) =  msg match {
      case TryPlayCard(stackId, card) => {
        if (card.canPutOnTopOf(tableState.stackStates.get(stackId).head)) {
          if (isPlayingCard) {
            self ! msg
          } else {
            playCard(card, stackId)
            isPlayingCard = true
          }
        } else {
          println("COMPUTERPLAYER:::::::::Retrying to play card "+card+" to "+stackId+" is not possible anymore")
        }
      }
      case TryPlayStartingCard(card) => {
        if (isPlayingCard) {
          self ! msg
        } else {
          playCard(card)
          isPlayingCard = true
        }
      }
  }
  
  private def getPlayableCardInOpenCards(card : Card) = {
    var playableCard = handSet.openCards.find {c => c != null && c.canPutOnTopOf(card)}
    if (playableCard != None) {
      playableCard.head
    } else {
      null
    }
  }
  
  private def getPlayableCardInHandCards(card : Card) = {
    var playableCard : Card = null
    if (handSet.revealedHandCard != null && handSet.revealedHandCard.canPutOnTopOf(card)) {
      playableCard = handSet.revealedHandCard
    }
    var i = 1
    while (i <= 4 && playableCard != null) {
      // TODO BROKEN
	  //handSet.setFirstRevealNextCardCount(i%3+1)
	  do {
	    println("scanning handCards... revealedCard: " + handSet.revealedHandCard)
	    // TODO BROKEN
	    //handSet.revealNextHandCard
	    if (handSet.revealedHandCard.canPutOnTopOf(card)) {
	    	playableCard = handSet.revealedHandCard
	    	println("found playable card: " + handSet.revealedHandCard)
	    }
	  } while (!handSet.isFirstRevealNextCard && playableCard != null)
	i += 1
    } 
    playableCard
  }
  
  private def playStartingCardsInHandCards = {
    if (handSet.revealedHandCard != null && handSet.revealedHandCard.number == Card.minNumber) {
      self ! new TryPlayStartingCard(handSet.revealedHandCard)
    }
    var i = 1
    while (i <= 4) {
      // TODO BROKEN
	  //handSet.setFirstRevealNextCardCount(i%3+1)
	  do {
	    println("scanning handCards... revealedCard: " + handSet.revealedHandCard)
	    // TODO BROKEN
	    //handSet.revealNextHandCard
	    if (handSet.revealedHandCard.number == Card.minNumber) {
	    	self ! new TryPlayStartingCard(handSet.revealedHandCard)
	    	println("found playable card: " + handSet.revealedHandCard)
	    }
	  } while (!handSet.isFirstRevealNextCard)
	i += 1
    }
  }

  def onHandSetRefresh() {
    // TODO fozzie bear
  }
}
