package com.zuehlke.ligretto.api.domain

import Color._

object Card {
  val minNumber = 1
  val maxNumber = 10
}

case class Card(playerId: Integer, color: Color, number: Integer) {

  override def toString = {
    "Card(" + playerId + "," + color + "," + number + ")"
  }
  
  def canEqual(that:Any) : Boolean = that match {
    case c: Card => true
    case _ => false
  }
  
  override def equals(that:Any): Boolean = {
    def strictEquals(other:Card) = 
      this.playerId == other.playerId &&
      this.color == other.color &&
      this.number == other.number
    that match {
      case a: AnyRef if this eq a => true
      case c: Card => (c canEqual this) && strictEquals(c)
      case _ => false
    }
  }
  
  def canPutOnTopOf(card: Card) = {
    this.color.equals(card.color) && this.number == card.number + 1
  }
}