package com.zuehlke.ligretto.api

import akka.actor.{Props, ActorSystem, ActorRef}
import com.zuehlke.ligretto.impl.actors.{TableStackSupervisor, PlayerSupervisor}

object GameFactory {

  private var _playerSupervisor: ActorRef = null
  private var _tableStackSupervisor: ActorRef = null
  private var _playerCount : Int = 4

  def playerCount = _playerCount

  def setPlayerCount(playerCount : Int) : Unit = { _playerCount = playerCount}

  def playerSupervisor(system: ActorSystem) = {
    if (_playerSupervisor == null) {
      _playerSupervisor = system.actorOf(Props( new PlayerSupervisor(playerCount)), PlayerSupervisor.name)
      _tableStackSupervisor = system.actorOf(Props[TableStackSupervisor], TableStackSupervisor.name)
    }
    _playerSupervisor
  }

}