package com.zuehlke.ligretto.api.actors

import com.zuehlke.ligretto.api.domain.Card

trait Move extends CustomMessage
case object OpenCardMove extends Move
case object HandCardMove extends Move
case object RevealNextCardMove extends Move

class FozzyPlayer extends PlayerBase {

  private var isPlayingCard = false
  private var handSetRefreshed = true

  def onDealCards = {
    // do nothing
  }

  def onLigrettoStart {
    println("STARTING MOVING")
    self ! OpenCardMove
    self ! HandCardMove
    self ! HandCardMove
    self ! HandCardMove
  }

  def onLigrettoStop = {}

  def onCardAccepted = {
    println("COMPUTERPLAYER::::::::CARD ACCEPTED")
    isPlayingCard = false
  }

  def onCardDenied = {
    println("COMPUTERPLAYER::::::::CARD DENIED")
    isPlayingCard = false
  }

  def onCardPlayed(stackId: Int, card: Card) = {
    // FozzyPlayer does not care
  }

  def onCustomMessage(msg: CustomMessage) = msg match {
    case OpenCardMove => {
      val targetFirstCard = handSet.openCards.find {
        c => c.number == Card.minNumber
      }
      if (targetFirstCard != None) {
        self ! new TryPlayStartingCard(targetFirstCard.head)
      }
      for (stackId <- tableState.stackStates.keys) {
        val stackCard = tableState.stackStates.get(stackId)
        if (stackCard != None) {

          val targetPushCard = handSet.openCards.find {
            c => c.canPutOnTopOf(stackCard.head)
          }
          if (targetPushCard != None) {
            self ! new TryPlayCard(stackId, targetPushCard.head)
          }
        }
      }
      self ! OpenCardMove
    }
    case HandCardMove => {
      if (handSet.revealedHandCard == null) {
        self ! RevealNextCardMove
        handSetRefreshed = false
      } else if (handSetRefreshed) {
        if (handSet.revealedHandCard.number == Card.minNumber) {
          self ! new TryPlayStartingCard(handSet.revealedHandCard)
        }
        for (stackId <- tableState.stackStates.keys) {
          val stackCard = tableState.stackStates.get(stackId)
          if (stackCard != None) {

            if (handSet.revealedHandCard.canPutOnTopOf(stackCard.head)) {
              self ! new TryPlayCard(stackId, handSet.revealedHandCard)
            }
          }
        }
        self ! RevealNextCardMove
      }
      self ! HandCardMove
    }

    case TryPlayCard(stackId, card) => {
      if (handSet.isPlayable(card) && card.canPutOnTopOf(tableState.stackStates.get(stackId).head)) {
        if (isPlayingCard || !handSetRefreshed) {
          //self ! msg
        } else {
          playCard(card, stackId)
          isPlayingCard = true
        }
      } else {
      }
    }
    case TryPlayStartingCard(card) => {
      if (isPlayingCard || !handSetRefreshed) {
        //self ! msg
      } else {
        if (handSet.isPlayable(card)) {
          playCard(card)
          isPlayingCard = true
        }
      }
    }
    case RevealNextCardMove => {
      handSetRefreshed = false
      revealNextCard
    }
  }

  def onHandSetRefresh = {
    handSetRefreshed = true
  }

}
