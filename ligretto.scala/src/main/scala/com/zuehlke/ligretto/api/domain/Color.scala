package com.zuehlke.ligretto.api.domain

object Color extends Enumeration {
  type Color = Value
  val Red, Green, Blue, Yellow = Value
}