package com.zuehlke.ligretto.impl.actors

import akka.actor.ActorContext

object ActorRegistry {

  private val root = "user/"

  def players(context: ActorContext) = {
    context.system.actorSelection(root + PlayerSupervisor.name + "/*")
  }

  def playerSupervisor(context: ActorContext) = {
    context.system.actorSelection(root + PlayerSupervisor.name)
  }

  def tableStackSV(context: ActorContext) = {
    context.system.actorSelection(root + TableStackSupervisor.name)
  }

  def tableStacks(context: ActorContext) = {
    context.system.actorSelection(root + TableStackSupervisor.name + "/*")
  }
}
