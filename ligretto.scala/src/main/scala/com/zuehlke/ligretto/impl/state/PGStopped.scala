package com.zuehlke.ligretto.impl.state

import akka.actor.ActorRef
import com.zuehlke.ligretto.impl.actors.PlayerGuard
import com.zuehlke.ligretto.api.actors.{LigrettoStop, MessageToPlayerGuardian}

class PGStopped(private val guard : PlayerGuard) extends PlayerGuardNoMoveStateHandler {

  guard.toPlayer(LigrettoStop)
  // TODO count and send!

  protected def doHandle(guard: PlayerGuard, sender: ActorRef, message: MessageToPlayerGuardian): PlayerGuardStateHandler = null

}
