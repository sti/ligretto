package com.zuehlke.ligretto.impl.actors

import akka.actor.{ActorRef, Props, Actor}

import com.zuehlke.ligretto.api.domain.{Color, Card}
import com.zuehlke.ligretto.impl.domain.PlayerHandSetImpl
import com.zuehlke.ligretto.api.actors._
import com.zuehlke.ligretto.api.actors.PlayerAccepted
import com.zuehlke.ligretto.api.actors.RequestJoin


object PlayerSupervisor {
  val name = "players"
  val maxPlayers = 8
}

class PlayerSupervisor(private val playerCount : Int) extends Actor {

  private var playersReady: Map[ActorRef, Boolean] = Map.empty
  private var started = false

  def receive = {
    case msg: PlayerRequest => {
      debug(msg+" received")
      onPlayerRequest(msg)
    }

  }

  private def onPlayerRequest(msg: PlayerRequest) = msg match {
    case RequestJoin(playerProps) => {
      debug("RequestJoin received.")
      // Check the maximum is not reached yet
      if (!started && playersReady.size < PlayerSupervisor.maxPlayers) {
        val playerId = playersReady.size
        debug("Accepting player "+playerId)
        val actor = context.actorOf(Props(new PlayerGuard(playerId, playerProps, sender)), "player_"+playerId)
        actor ! new DealCardsEvent(createHandSet(playerId))
        playersReady += actor -> false
        sender ! new PlayerAccepted(actor)
      } else {
        debug("Denied player, we are full or have started already.")
        sender ! PlayerDenied
      }
    }
    case RequestStart => {
      if (!started && playersReady.contains(sender)) {
        debug("Player requested start...")
        playersReady += sender -> true
        // Check if a player is not ready yet, if not, start the game
        val notReady = playersReady.values.find {
          r => !r
        }
        // Start the game, if all players are ready and more than one is connected
        if (playersReady.size == playerCount && notReady == None) {
          debug("All players have requested start, sending ligretto start...")
            started = true
          ActorRegistry.tableStackSV(context) ! LigrettoStartEvent
          // Send start event to all players
          ActorRegistry.players(context) ! LigrettoStartEvent
        }
      } else {
        debug("Unknown player "+sender)
      }
    }
  }

  private def debug(msg : String) : Unit = {
    println("PlayerSupervisor: "+msg)
  }

  private def createHandSet(playerId: Int) = {
	  var handSet = new PlayerHandSetImpl()
	  handSet.initializeHandSet(createCardSet(playerId).toList)
	  handSet    
  }

  private def createCardSet(playerId: Int): Set[Card] = {
    for (
      color <- Color.values;
      number <- Card.minNumber to Card.maxNumber
    ) yield new Card(int2Integer(playerId), color, int2Integer(number))
  }

}
