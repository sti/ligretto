package com.zuehlke.ligretto.impl.domain

import com.zuehlke.ligretto.api.domain.Card
import com.zuehlke.ligretto.api.domain.TableState

class TableStateImpl extends TableState {

  private var _stackCards: Map[Int, Card] = Map.empty

  def stackStates = {
    _stackCards
  }

  def refresh(id: Int, card: Card): TableStateImpl = {
    val result = new TableStateImpl()
    result._stackCards = _stackCards + (id -> card)
    return result
  }

}
