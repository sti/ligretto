package com.zuehlke.ligretto.impl.actors

import akka.actor.Actor
import scala.collection.immutable.Stack
import com.zuehlke.ligretto.api.domain.Card

class TableStack(private val initialCard: Card, private val id: Int) extends Actor {

  require(initialCard.number == Card.minNumber)
  private var cards = Stack(initialCard)
  notifyPlayers

  private var accepting = true
  def topCard = cards.top

  def receive = {
    case PushCardMove(pushCard: Card) => {
      println("PushCardMove received for " + pushCard)
      if (accepting && cards.head.color == pushCard.color &&
        (cards.head.number + 1) == pushCard.number) {
        println("Accepting push for " + pushCard)
        cards = cards.push(pushCard)
        sender ! CardAcceptedResult
        notifyPlayers
      } else {
        println("Rejecting push for " + pushCard + " accepting="+accepting+" topCard="+cards.head)
        sender ! CardDeniedResult
      }
    }
    case LigrettoStopEvent => {
      accepting = false // No more cards
      // TODO cards.map( )
    }
  }

  def notifyPlayers: Unit = {
    println("Notifying refresh of stack with card " + cards.top)
    ActorRegistry.players(context) ! TableStackRefreshEvent(cards.top, id)
  }
}
