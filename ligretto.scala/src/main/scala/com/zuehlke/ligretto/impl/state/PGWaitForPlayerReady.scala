package com.zuehlke.ligretto.impl.state

import akka.actor.ActorRef
import com.zuehlke.ligretto.impl.actors.{PlayerGuard, ActorRegistry}
import com.zuehlke.ligretto.api.actors.{CardDenied, PlayCard, MessageToPlayerGuardian, RequestStart}

object PGWaitForPlayerReady extends PlayerGuardStateHandler {

  def doHandle(guard: PlayerGuard, sender: ActorRef, message: MessageToPlayerGuardian) = message match {
    case RequestStart => {
      debug(guard, "received request start from player")
      guard.toPlayerSupervisor(RequestStart)
      PGWaitForLigrettoStart
    }
    case PlayCard(stackId, card) => {
      debug(guard, "denying a play card "+card+" to stack "+stackId)
      sender ! CardDenied
      this
    }
    case _ => {
      debug(guard, "skipped message "+message)
      this
    }
  }

  override def handleRefresh(guard: PlayerGuard, sender: ActorRef, message : MessageToPlayerGuardian) = { null }

}
