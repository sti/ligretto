package com.zuehlke.ligretto.impl.state

import akka.actor.ActorRef
import com.zuehlke.ligretto.impl.actors.{PlayerGuard, DealCardsEvent}
import com.zuehlke.ligretto.api.actors.{PlayCard, MessageToPlayerGuardian, DealCards}

object PGStart extends PlayerGuardNoMoveStateHandler {

  def doHandle(guard: PlayerGuard, sender: ActorRef, message: MessageToPlayerGuardian) = message match {
    case DealCardsEvent(initialCards) => {
      debug(guard, "received deal cards message")
      guard.handSet = initialCards
      // Send event to player
      guard.toPlayer(new DealCards(initialCards))
      PGWaitForPlayerReady
    }
    case _ => { null }
  }

  override def handleRefresh(guard: PlayerGuard, sender: ActorRef, message : MessageToPlayerGuardian) = { null }
}
