package com.zuehlke.ligretto.impl.state

import com.zuehlke.ligretto.impl.actors.{LigrettoStopEvent, CardDeniedResult, CardAcceptedResult, PlayerGuard}
import com.zuehlke.ligretto.api.domain.Card
import com.zuehlke.ligretto.api.actors.{LigrettoStop, CardAccepted, CardDenied, MessageToPlayerGuardian}
import akka.actor.ActorRef

trait PGAcceptDenyHandler extends PlayerGuardNoMoveStateHandler  {

  def handleAccepted(guard : PlayerGuard, playedCard : Card) {
    debug(guard, "Card was accepted: " + playedCard)
    guard.handSet = guard.handSet.cardPlayed(playedCard)
    guard.toPlayer(new CardAccepted(guard.handSet))
  }

  def handleDenied(guard : PlayerGuard, playedCard : Card) {
    debug(guard, "Card was rejected: " + playedCard)
    guard.toPlayer(CardDenied)
  }
}

class PGWaitForAcceptDeny(private val playedCard : Card) extends PGAcceptDenyHandler {
  protected def doHandle(guard: PlayerGuard, sender: ActorRef, message: MessageToPlayerGuardian): PlayerGuardStateHandler = message match {
    case CardAcceptedResult => {
      handleAccepted(guard, playedCard)
      PGWaitForMove
    }
    case CardDeniedResult => {
      handleDenied(guard, playedCard)
      PGWaitForMove
    }
    case LigrettoStopEvent => {
      debug(guard, "Ligretto Stop received")
      guard.toPlayer(LigrettoStop)
      new PGStoppedWaitForAcceptDeny(playedCard)
    }
    case _ => { null }
  }
}

class PGStoppedWaitForAcceptDeny(private val playedCard : Card) extends PGAcceptDenyHandler {
  protected def doHandle(guard: PlayerGuard, sender: ActorRef, message: MessageToPlayerGuardian): PlayerGuardStateHandler = message match {
    case CardAcceptedResult => {
      handleAccepted(guard, playedCard)
      new PGStopped(guard)
    }
    case CardDeniedResult => {
      handleDenied(guard, playedCard)
      new PGStopped(guard)
    }
    case _ => { null }
  }
}
