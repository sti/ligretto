package com.zuehlke.ligretto.impl.domain

import com.zuehlke.ligretto.api.domain.{ HandSet, Card }
import scala.util.Random

class PlayerHandSetImpl() extends HandSet {

  private var _stack: List[Card] = null
  private var _openCards: List[Card] = null
  private var _handCards: List[Card] = null
  private var _revealedHandCardIdx = -1
  private var _firstRevealHandCardCount = 1

  def initializeHandSet(cards: List[Card]) {
    var initialCards = shuffleCards(cards)
	while (isCheckSumToHigh(initialCards) || !hasPossibleFirstMove(initialCards)) {
		initialCards = shuffleCards(initialCards)
	}
	setUpStacks(initialCards)
  }

  def cardPlayed(card: Card): PlayerHandSetImpl = {
    val result = this.copy
    result._handCards = result._handCards diff List(card)
    result._openCards = result._openCards diff List(card)
    if (result._openCards.size < 3) {
      result._openCards ::= result._stack.head
      result._stack = result._stack.tail
    } else {
      result._revealedHandCardIdx -= 1
    }
    result
  }

  def revealNextHandCard: PlayerHandSetImpl = {
    val result = this.copy
    var indexIncrease = 3
    if (isFirstRevealNextCard) {
      indexIncrease = result._firstRevealHandCardCount
      result._revealedHandCardIdx = 0
    }
    result._revealedHandCardIdx = Math.min(result._revealedHandCardIdx + indexIncrease, result._handCards.size - 1)
    result
  }

  def revealedHandCard = {
    if (_revealedHandCardIdx < 0) {
      null
    } else {
      _handCards.drop(_revealedHandCardIdx).head
    }
  }

  def handCardSize = { _handCards.size }

  def stackSize = { _stack.size }

  def topStackCard = { _stack.head }

  def openCards = { _openCards }

  def firstRevealHandCardCount = { _firstRevealHandCardCount }

  def setFirstRevealNextCardCount(count: Int): PlayerHandSetImpl = {
    require(count >= 0 && count <= 3)
    val result = this.copy
    result._firstRevealHandCardCount = count
    result
  }

  def isFirstRevealNextCard = {
    _revealedHandCardIdx == _handCards.size - 1 || _revealedHandCardIdx == -1
  }

  def isPlayable(card: Card) = {
    _openCards.exists(c => c.equals(card)) || card.equals(revealedHandCard)
  }

  private def isCheckSumToHigh(initialCards : List[Card]) = {
    var sum = checkSum(initialCards.head.number)
    for (i <- 0 to 2) {
      sum += checkSum(initialCards.drop(i + 10).head.number)
    }
    sum >= 30
  }

  private def hasPossibleFirstMove(initialCards : List[Card]) = {
    initialCards.drop(10).exists(c => c.number == Card.minNumber)
  }

  private def checkSum(number: Int) = {
    var num = number
    var checkSum = 0
    while (num > 9) {
      checkSum += (num / 10) % 10
      num = num / 10
    }
    checkSum += num
    checkSum
  }

  private def shuffleCards(initialCards : List[Card]) = {
    Random.shuffle(initialCards)
  }

  private def setUpStacks(initialCards : List[Card]) = {
    _stack = initialCards.take(10)
    _openCards = initialCards.drop(10).take(3)
    _handCards = initialCards.drop(13)
  }

  private def copy: PlayerHandSetImpl = {
    val copy = new PlayerHandSetImpl
    copy._firstRevealHandCardCount = this._firstRevealHandCardCount
    copy._handCards = this._handCards
    copy._openCards = this._openCards
    copy._revealedHandCardIdx = this._revealedHandCardIdx
    copy._stack = this._stack
    copy
  }
}
