package com.zuehlke.ligretto.impl.actors

import akka.actor.{Props, Actor}
import com.zuehlke.ligretto.api.domain.Card

object TableStackSupervisor {
  val name = "table"
}

class TableStackSupervisor extends Actor {

  private var accepting = false
  private var idCounter = 0

  def receive = {
    case LigrettoStartEvent => {
      println("Starting game")
      accepting = true
    }
    case CreateStackMove(initialCard) => {
      if (accepting && initialCard.number == Card.minNumber) {
        println("Creating table stack actor for " + initialCard + " with stackId " + idCounter)
        sender ! CardAcceptedResult
        context.actorOf(Props(new TableStack(initialCard, idCounter)))
        idCounter += 1
      } else {
        println("Rejecting, cause: accept=" + accepting + ", card="+initialCard)
        sender ! CardDeniedResult
      }
    }
    case LigrettoStopEvent => {
      accepting = false
      // After ensuring no new stacks may be created, send the event to the existing ones
      ActorRegistry.tableStacks(context) ! LigrettoStopEvent
    }
  }

}
