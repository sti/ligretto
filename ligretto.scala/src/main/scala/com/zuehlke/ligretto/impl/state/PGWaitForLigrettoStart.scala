package com.zuehlke.ligretto.impl.state

import akka.actor.ActorRef
import com.zuehlke.ligretto.impl.actors.{PlayerGuard, LigrettoStartEvent}
import com.zuehlke.ligretto.api.actors.{MessageToPlayerGuardian, LigrettoStart}

object PGWaitForLigrettoStart extends PlayerGuardNoMoveStateHandler {

  def doHandle(guard: PlayerGuard, sender: ActorRef, message: MessageToPlayerGuardian) = message match {
    case LigrettoStartEvent => {
      debug(guard, "received LigrettoStartEvent")
      guard.toPlayer(new LigrettoStart(guard.table))
      PGWaitForMove
    }
    case _ => { null }
  }

  override def handleRefresh(guard: PlayerGuard, sender: ActorRef, message : MessageToPlayerGuardian) = { null }
}
