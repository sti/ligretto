package com.zuehlke.ligretto.impl.state

import akka.actor.ActorRef
import com.zuehlke.ligretto.api.domain.Card
import com.zuehlke.ligretto.impl.actors.{TableStackRefreshEvent, PlayerGuard}
import com.zuehlke.ligretto.api.actors._
import com.zuehlke.ligretto.impl.actors.TableStackRefreshEvent
import com.zuehlke.ligretto.api.actors.PlayCard
import com.zuehlke.ligretto.api.actors.CardPlayed

trait PlayerGuardStateHandler {

  def handle(guard: PlayerGuard, sender: ActorRef, message : MessageToPlayerGuardian) : PlayerGuardStateHandler = {
    var result = doHandle(guard, sender, message)
    if (result == null) {
      result = handleRefresh(guard, sender, message)
    }
    if (result == null) {
      debug(guard, "ignored "+message)
      this
    } else {
      result
    }
  }

  protected def handleRefresh(guard: PlayerGuard, sender: ActorRef, message : MessageToPlayerGuardian) : PlayerGuardStateHandler = message match {
    case SetFirstCard(idx) => {
      guard.handSet = guard.handSet.setFirstRevealNextCardCount(idx)
      guard.toPlayer(new HandSetRefresh(guard.handSet))
      this
    }
    case RevealNextCard => {
      guard.handSet = guard.handSet.revealNextHandCard
      guard.toPlayer(new HandSetRefresh(guard.handSet))
      this
    }
    case TableStackRefreshEvent(topCard, stackId) => {
      debug(guard, "Received refresh for "+guard.id+" with card " + topCard+ " in stack "+stackId)
      guard.stacks += (stackId -> sender)
      guard.table = guard.table.refresh(stackId, topCard)
      guard.toPlayer(new CardPlayed(stackId, topCard, guard.table))
      this
    }
    case _ => { null }
  }

  protected def doHandle(guard: PlayerGuard, sender: ActorRef, message : MessageToPlayerGuardian) : PlayerGuardStateHandler

  protected def debug(guard: PlayerGuard, msg: String) : Unit = {
    println(this.getClass.getName+"(Guard"+guard.id+"): "+msg)
  }

}

trait PlayerGuardNoMoveStateHandler extends PlayerGuardStateHandler {

   override def handle(guard: PlayerGuard, sender: ActorRef, message : MessageToPlayerGuardian) : PlayerGuardStateHandler = message match {
     case SetFirstCard(idx) => {
       this
     }
     case RevealNextCard => {
       this
     }
     case PlayCard(stackId, card) => {
       debug(guard, "denied play of card "+card+" to "+stackId+" due to in no state to play")
       sender ! CardDenied
       this
     }
     case _ => {
       super.handle(guard, sender, message)
     }
   }

}