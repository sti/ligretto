package com.zuehlke.ligretto.impl.state

import com.zuehlke.ligretto.impl.actors._
import com.zuehlke.ligretto.api.actors.{LigrettoStop, CardDenied, PlayCard, MessageToPlayerGuardian}
import akka.actor.ActorRef
import com.zuehlke.ligretto.impl.actors.PushCardMove
import com.zuehlke.ligretto.api.actors.PlayCard
import com.zuehlke.ligretto.impl.actors.CreateStackMove

object PGWaitForMove extends PlayerGuardStateHandler {

  def doHandle(guard: PlayerGuard, sender: ActorRef, message: MessageToPlayerGuardian): PlayerGuardStateHandler = message match {
    case PlayCard(stackId, card) => {
      debug(guard, "received play of card "+card+" to "+stackId+" with hand set "+guard.handSet)
      if (guard.handSet.isPlayable(card)) {
        if (stackId == null) {
          debug(guard, "Sending card "+card+" to table stack supervisor...")
          guard.toStackSupervisor(new CreateStackMove(card))
        } else {
          debug(guard, "Sending card "+card+" to table stack...")
          guard.toStack(Integer2int(stackId), new PushCardMove(card))
        }
        new PGWaitForAcceptDeny(card)
      } else {
        debug(guard, "card "+card+" to "+stackId+" denied, it is not playable!")
        sender ! CardDenied
        this
      }
    }
    case LigrettoStop => {
      guard.toStackSupervisor(LigrettoStopEvent) // To players
      guard.toPlayers(LigrettoStopEvent) // To players
      new PGStopped(guard)
    }
    case LigrettoStopEvent => {
       debug(guard, "Ligretto Stop Event received")
       new PGStopped(guard)
    }
    case _ => { null }
  }
}
