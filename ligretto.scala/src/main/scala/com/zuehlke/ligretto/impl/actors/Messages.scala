package com.zuehlke.ligretto.impl.actors

import com.zuehlke.ligretto.api.domain.Card
import com.zuehlke.ligretto.impl.domain.PlayerHandSetImpl
import akka.actor.ActorRef
import com.zuehlke.ligretto.api.actors.MessageToPlayerGuardian

sealed trait TableCommand
case class CreateStackMove(initialCard: Card) extends TableCommand
case class PushCardMove(pushCard: Card) extends TableCommand

sealed trait GameEvent extends MessageToPlayerGuardian
case object LigrettoStartEvent extends GameEvent
case object LigrettoStopEvent extends GameEvent
case class DealCardsEvent(initialCards: PlayerHandSetImpl) extends GameEvent
case class TableStackRefreshEvent(topCard: Card, stackId: Int) extends GameEvent
case class CountCards(playerAndCount : Map[Int, Int]) extends GameEvent

sealed trait MoveResult extends MessageToPlayerGuardian
case object CardAcceptedResult extends MoveResult
case object CardDeniedResult extends MoveResult

