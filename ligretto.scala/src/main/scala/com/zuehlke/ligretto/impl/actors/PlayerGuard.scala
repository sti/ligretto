package com.zuehlke.ligretto.impl.actors

import akka.actor.{Props, Actor, ActorRef}
import com.zuehlke.ligretto.api.domain.Card
import com.zuehlke.ligretto.impl.domain.{PlayerHandSetImpl, TableStateImpl}
import com.zuehlke.ligretto.api.actors._
import com.zuehlke.ligretto.impl.state.{PGStart, PlayerGuardStateHandler}

class PlayerGuard(val id: Int, private val playerProps: Props,
                  private val creator: ActorRef) extends Actor {

  var cardSent: Card = null

  var gameIsOn = false

  var table = new TableStateImpl

  var handSet: PlayerHandSetImpl = null

  var stacks: Map[Int, ActorRef] = Map.empty

  // Create the guarded player
  private val player = context.actorOf(playerProps)

  // Notify the creator, the player has been accepted
  // and provide the actor reference
  creator ! new PlayerAccepted(player)

  def toPlayerSupervisor(msg: AnyRef) = {
    ActorRegistry.playerSupervisor(context) ! msg
  }
  def toPlayers(msg: AnyRef) = {
    ActorRegistry.players(context) ! msg
  }
  def toStackSupervisor(msg: AnyRef) = {
    ActorRegistry.tableStackSV(context) ! msg
  }
  def toStack(stackId: Int, msg: AnyRef) = {
    val actor = stacks.get(stackId)
    if (actor != None) { actor.head ! msg }
  }
  def toPlayer(msg : AnyRef) = { player ! msg }

  private var state : PlayerGuardStateHandler = PGStart

  def receive = {
    case msg : MessageToPlayerGuardian => {
      state = state.handle(this, sender, msg)
    }
    case msg: AnyRef => {
      println("PlayerGuard("+id+"): ignored "+msg)
    }
  }
}
